# Tetrinet client with React Native

Alunos

  - Fernando Severino Almeida (líder) - fernandoseverino@discente.ufg.br
  - Vinicius Barreto Costa - viniiciusbk@gmail.com
  - Arlley Matheus Pereira da Silva Lima(Suporte técnico)- arlleymatheus10@hotmail.com

# Install

```sh
$ npm install -g create-react-native-app
$ npm install -g expo-cli
```
* clone the project
```sh
$ cd frontend-rn-g1
$ npm install
$ npm start
```
* Download expo on Play Store or App Store
* scan the QR Code on app EXPO in your smartphone
* if you followed all the steps you will see the Tetris home screen. :star2: :heart:

###### IF YOU ARE USING YOUR PHONE, MAKE SURE YOUR PHONE AND COMPUTER ARE ON THE SAME LOCAL NETWORK


### Warning:
  ##### Change the proxy ip on GameMenu.js if using EXPO on physical device or use ip = "localhost" if using EXPO in virtual device, e.g. Genymotion

License
----

MIT