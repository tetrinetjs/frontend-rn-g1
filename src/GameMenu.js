import React, { useState, useEffect } from 'react';
import {
    SafeAreaView,
    Alert,
    Text,
    TouchableOpacity,
    StyleSheet,
    Image,
    AsyncStorage
} from 'react-native';

//animacoes
import * as Animatable from 'react-native-animatable';

import logo from './assets/logo.png';

import BestScores from './Components/BestScores';

//LIB from websocket
import { w3cwebsocket as W3CWebSocket } from "websocket";

//Warning: change the ip if using EXPO on phisical device or use ip with localhost if using EXPO in virtual device ex.: Genymotion
const client = new W3CWebSocket('ws://192.168.100.2:1337');

export default function GameMenu({navigation}){
    const[gameOver, setGameOver] = useState(true);
    const[bestScores, setBestScores] = useState(null);

    useEffect(() => {
        async function scores(){
            try{
                let data = await AsyncStorage.getItem('Best_Scores')
                setBestScores(JSON.parse(data));
                setGameOver(false);
            }catch(e){
                console.log(e);
            }
        }

        if(gameOver == true){
            scores();
        }
    }, [gameOver]);

    //Connecting to proxy Tetrinet
    useEffect(() => {
        //Opening connection
        client.onopen = () => {
            console.log('WebSocket Client Connected');
            client.send(`connect localhost 31457`)
        };

        //Messages received
        client.onmessage = (message) => {
            console.log(message.data);
        };
    }, []);

    return(
        <SafeAreaView style={styles.container}>
            <Image source={logo} style={styles.logo}/>

            <Animatable.View animation="rubberBand" easing="ease-out" iterationCount="infinite">
                <TouchableOpacity style={styles.btnStart} onPress={() => navigation.navigate('Game', { setGameOver })}>
                    <Text style={styles.txtBtnStart}>Start</Text>
                </TouchableOpacity>
            </Animatable.View>

            <BestScores data={bestScores}></BestScores>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
   container: {
       flex: 1,
       backgroundColor: '#006400',
       alignItems: 'center',
       justifyContent: 'center'
   },

   logo: {
       width: 200,
       height: 40,
       marginBottom: 30
   },

   btnStart: {
       backgroundColor: '#FFF'
   },

   txtBtnStart: {
       fontSize: 20,
       color: '#006400',
       paddingHorizontal: 30,
       paddingVertical: 20
   }
});